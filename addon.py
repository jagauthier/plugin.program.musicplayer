import os
import xbmc
import xbmcgui
import xbmcaddon
import threading
import time
import subprocess
import dbus
import dbus.service

###################################################################################################
###################################################################################################
# Initialization
###################################################################################################
###################################################################################################
ACTION_PREVIOUS_MENU = 10
ACTION_SELECT_ITEM = 7

TEXT_ALIGN_LEFT = 0
TEXT_ALIGN_RIGHT = 1
TEXT_ALIGN_CENTER_X = 2
TEXT_ALIGN_CENTER_Y = 4
TEXT_ALIGN_RIGHT_CENTER_Y = 5
TEXT_ALIGN_LEFT_CENTER_X_CENTER_Y = 6

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'

def log(logline):
    print "MPLAYER: " + logline
    
def set_kodi_prop(property, value):
    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value


# Get global paths
addon = xbmcaddon.Addon(id = "plugin.program.musicplayer")
resourcesPath = os.path.join(addon.getAddonInfo('path'),'resources') + '/'
mediaPath = os.path.join(addon.getAddonInfo('path'),'resources','media') + '/'

carPCskin = xbmcaddon.Addon(id = "skin.CarPC-touch_carbon")
carPCbg=os.path.join(carPCskin.getAddonInfo('path'),'background') + '/'

addonW = 1280
addonH = 720

title_label = xbmcgui.ControlLabel(150,125,addonW-(addonW/4)-225,64,'',textColor='0xffffffff',font='font36',alignment=TEXT_ALIGN_CENTER_Y)
album_label = xbmcgui.ControlLabel(150,200,addonW-(addonW/4)-225,64,'',textColor='0xffffffff',font='font36',alignment=TEXT_ALIGN_CENTER_Y)
artist_label = xbmcgui.ControlLabel(150,275,addonW-(addonW/4)-225,64,'',textColor='0xffffffff',font='font36',alignment=TEXT_ALIGN_CENTER_Y)
genre_label = xbmcgui.ControlLabel(150,350,addonW,64,'',textColor='0xffffffff',font='font36',alignment=TEXT_ALIGN_CENTER_Y)
clock_label = xbmcgui.ControlLabel(100,0,addonW,83,'',textColor='0xffffffff',font='font30',alignment=TEXT_ALIGN_CENTER_Y)

pos_label = xbmcgui.ControlLabel(50,425,addonW,83,'',textColor='0xffffffff',font='font12',alignment=TEXT_ALIGN_CENTER_Y)
tr_label = xbmcgui.ControlLabel(325,425,addonW,83,'',textColor='0xffffffff',font='font12',alignment=TEXT_ALIGN_CENTER_Y)

title_icon = xbmcgui.ControlImage(50, 125, 64, 64, mediaPath + "title.png")
album_icon = xbmcgui.ControlImage(50, 200, 64, 64, mediaPath + "album.png")
artist_icon = xbmcgui.ControlImage(50, 275, 64, 64, mediaPath + "artist.png")
genre_icon = xbmcgui.ControlImage(50, 350, 64, 64, mediaPath + "genre.png")

album_img = xbmcgui.ControlImage(addonW-(addonW/4)-75, 175, addonW/4, addonH/4, "")
artist_img = xbmcgui.ControlImage(addonW-(addonW/4)-75, 225+addonH/4, addonW/4, addonH/4, "")

track_progress = xbmcgui.ControlSlider(50, 435, 1, 10, textureback=mediaPath + "progress.png", texture='', texturefocus='')
progress_bg=xbmcgui.ControlImage(50, 430, 325, 20, mediaPath + "bottom_bar.png")
progress_fg=xbmcgui.ControlImage(50, 430, 10, 20, mediaPath + "progress_fg.png")

reverse_icon=xbmcgui.ControlButton(50, 480, 64, 64, '', \
                                   noFocusTexture=mediaPath + "reverse.png", \
                                   focusTexture=mediaPath + "reverse.png")

play_icon=xbmcgui.ControlButton(180, 480, 64, 64, '', \
                                noFocusTexture=mediaPath + "play.png", \
                                focusTexture=mediaPath + "play.png")
                                
pause_icon=xbmcgui.ControlButton(180, 480, 64, 64, '', \
                                 noFocusTexture=mediaPath + "pause.png", \
                                 focusTexture=mediaPath + "pause.png")
                                 
forward_icon=xbmcgui.ControlButton(310, 480, 64, 64, '', \
                                   noFocusTexture=mediaPath + "forward.png", \
                                   focusTexture=mediaPath + "forward.png")

player=None

def converttime(ms):
    s=ms/1000 
    m,s=divmod(s,60) 
    h,m=divmod(m,60) 
    d,h=divmod(h,24) 
    return d,h,m,s 

def mstotime(d,h,m,s):
    position=""
    if d>0:
        if d<10:
            position+="0"
        position+=str(d)+":"
        
    if h>0:
        if h<10:
            position+="0"
        position+=str(h)+":"

    if m<10:
        position+="0"
    position+=str(m)+":"
                
    if s<10:
        position+="0"
    position+=str(s)
    return position

def update_player_info():
    title=get_kodi_prop("player_title")
    artist=get_kodi_prop("player_artist")
    album=get_kodi_prop("player_album")
    genre=get_kodi_prop("player_genre")
    
    if title=="":
        title="No data"
    if artist=="":
    	artist="No data"
    if album=="":
        album="No data"
    if genre=="":
        genre="No data"
    title_label.setLabel(title)
    artist_label.setLabel(artist)
    album_label.setLabel(album)
    genre_label.setLabel(genre)
    
    update_cover_art(artist,album)

def update_cover_art(artist, album):
    # remove the / if any, and lowercase to match the services backend
    artist=artist.replace("/", "_").lower()
    album=album.replace("/", "_").lower()
    album_art=get_kodi_prop(artist+"/"+album)
    artist_art=get_kodi_prop("albumart/"+artist)
    
    album_img.setImage(album_art)
    artist_img.setImage(artist_art)
    

def update_track_position():
    d=0
    h=0
    m=0
    s=0
    pos_ms=0
    duration_ms=0
    tr_ms=0
    width=1
    # If there isn't a device connected, don't do any of this. It'll be fine.
    bt_device=get_kodi_prop("connected_device")
    if bt_device!="":
        pos_ms=int(player.Get(PLAYER_IFACE, "Position", dbus_interface="org.freedesktop.DBus.Properties"))
        duration_ms=int(get_kodi_prop("player_duration"))
        tr_ms=duration_ms-pos_ms
        
        if tr_ms<0:
            tr_ms=0
        log("Track remains: " + str(tr_ms))
        d,h,m,s=converttime(pos_ms)
        pos_label.setLabel(mstotime(d,h,m,s))
        d,h,m,s=converttime(tr_ms)
        tr_label.setLabel("-"+mstotime(d,h,m,s))
        track_progress.setPercent((pos_ms*100)/duration_ms)
        width=325*((pos_ms*100)/duration_ms)/100
        track_progress.setWidth(width)
        progress_fg.setPosition(width+45, progress_fg.getY())
    
previous_artist=None
previous_album=None
previous_title=None
previous_status=None
previous_album_art=None
previous_artist_art=None

def notable_change():
    global previous_album
    global previous_artist
    global previous_title
    global previous_status
    global previous_album_art
    global previous_artist_art
    
    title=get_kodi_prop("player_title")
    artist=get_kodi_prop("player_artist")
    album=get_kodi_prop("player_album")
    status=get_kodi_prop("player_status")
    
    artist=artist.replace("/", "_").lower()
    album=album.replace("/", "_").lower()
    
    album_art=get_kodi_prop(artist+"/"+album)
    artist_art=get_kodi_prop("albumart/"+artist)

    if status=="playing":
        pause_icon.setVisible(True)
        play_icon.setVisible(False)
    if status=="paused":
        pause_icon.setVisible(False)
        play_icon.setVisible(True)
    if status=="stopped" or status=="":
        pause_icon.setVisible(False)
        play_icon.setVisible(True)

    if title!=previous_title or \
       artist!=previous_artist or \
       album!=previous_album or \
       status!=previous_status or \
       album_art!=previous_album_art or \
       artist_art!=previous_artist_art:
           previous_title=title
           previous_artist=artist
           previous_album=album
           previous_status=status
           previous_artist_art=artist_art
           previous_album_art=album_art
           log("Notable change detected")
           return 1
    else:
        return 0

class updateStatus(threading.Thread):
    def run(self):
#        global player
        self.shutdown = False
        while not self.shutdown:

            update_track_position()
            
            if notable_change():
                update_player_info()
                    
            clock_label.setLabel(time.strftime("%I:%M%p").lstrip('0'))
            
            time.sleep(1)

class musicplayer(xbmcgui.WindowDialog):

    def __init__(self):
        global player
        self.w = addonW
        self.h = addonH
        # Background
        # TODO reference the skin background using Getlabel(Skin.HasSetting)
        self.background=xbmcgui.ControlImage(0, 0, self.w, self.h, carPCbg + "SKINDEFAULT.jpg")
        self.addControl(self.background)
        # top and bottom images
        self.addControl(xbmcgui.ControlImage(0, 0, self.w, 90, mediaPath + "top_bar.png"))
        self.addControl(xbmcgui.ControlImage(0, self.h - 90, self.w, 90, mediaPath + "bottom_bar.png"))
        #TITLE
        self.addControl(xbmcgui.ControlLabel(
            0, 15,
            self.w, 83,
            'Music Player',
            textColor='0xffffffff',
            font='font40',
            alignment=TEXT_ALIGN_CENTER_X))

            
        # back button
        self.button_back=xbmcgui.ControlButton(0, 0, 83, 83, "", "floor_buttonFO.png", "floor_button.png", 0, 0)
        self.addControl(self.button_back)
        self.addControl(xbmcgui.ControlImage(0, 0, 83, 83, "icon_back_w.png"))
        
        #text labels
        self.addControl(clock_label)
        self.addControl(title_label)
        self.addControl(artist_label)
        self.addControl(album_label)
        self.addControl(genre_label)
        self.addControl(pos_label)
        self.addControl(tr_label)
        
        # Progress bar
        self.addControl(progress_bg)
        self.addControl(track_progress)
        self.addControl(progress_fg)        

        # images
        self.addControl(title_icon)
        self.addControl(album_icon)
        self.addControl(artist_icon)
        self.addControl(genre_icon)        

        self.addControl(album_img)
        self.addControl(artist_img)

        self.addControl(play_icon)
        self.addControl(forward_icon)
        self.addControl(reverse_icon)
        self.addControl(pause_icon)        
        
        # do dbus connectivity
        self.bus = dbus.SystemBus()
        self.manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = self.manager.GetManagedObjects()

        
        set_kodi_prop("media_player_active", "1")
            
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                self.adapter=self.bus.get_object("org.bluez", path)
                self.adapter_manager=dbus.Interface(self.adapter, 'org.freedesktop.DBus.Properties')
                
            if PLAYER_IFACE in interfaces:
                player_path = path
                player = self.bus.get_object("org.bluez", player_path)
                

        self.updateThread = updateStatus()
        self.updateThread.start()        
        
    def onAction(self, action):
        log("ONACTION")        
        if action == ACTION_PREVIOUS_MENU:
            set_kodi_prop("media_player_active", "")
            log("ENDING.")            
            self.shutdown = True
            self.updateThread.shutdown = True
            self.updateThread.join()
            self.close()
            
    def onControl(self, controlID):
        log("ONCONTROL")
                    
        # Back button
        if controlID == self.button_back:
            set_kodi_prop("media_player_active", "")
            log("ENDING.")            
            self.shutdown = True
            self.updateThread.shutdown = True
            self.updateThread.join()
            self.close()            
        # don't do anything if no BT connection
        bt_device=get_kodi_prop("connected_device")
        if bt_device!="":
            if controlID == play_icon:
                player.Play(dbus_interface=PLAYER_IFACE)
                pause_icon.setVisible(True)
                play_icon.setVisible(False)            

            if controlID == pause_icon:
                player.Pause(dbus_interface=PLAYER_IFACE)
                pause_icon.setVisible(False)
                play_icon.setVisible(True)

            if controlID == forward_icon:
                player.Next(dbus_interface=PLAYER_IFACE)
                pause_icon.setVisible(False)
                play_icon.setVisible(True)

            if controlID == reverse_icon:
                player.Previous(dbus_interface=PLAYER_IFACE)
                pause_icon.setVisible(False)
                play_icon.setVisible(True)
                
# Start the Addon

dialog = musicplayer()
dialog.doModal()
del dialog
del addon
